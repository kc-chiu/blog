# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'd3eef8cb5a061392c6836d5d5d491a211483f8401324ddaa5dd52beede4dc7c2d2be5299e67051c0139972b238cdce78772217d4e445ab7c6596ac75fd232d91'
